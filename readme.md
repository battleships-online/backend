How to create suitable DB:
1. CREATE DATABASE battleships;
2. CREATE ROLE battleships WITH LOGIN PASSWORD 'password';
3. GRANT ALL PRIVILEGES ON DATABASE battleships TO battleships;