package net.battleships.mapper.handler

import net.battleships.domain.Coordinates
import net.battleships.domain.Point
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedJdbcTypes
import org.apache.ibatis.type.MappedTypes
import org.apache.ibatis.type.TypeHandler
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedJdbcTypes(JdbcType.ARRAY)
@MappedTypes(Coordinates::class)
@Suppress("UNCHECKED_CAST", "UNUSED")
class CoordinatesTypeHandler : TypeHandler<Coordinates> {
    override fun setParameter(
        ps: PreparedStatement,
        i: Int,
        parameter: Coordinates,
        jdbcType: JdbcType?
    ) {
        val jdbcArray = ps.connection.createArrayOf(
            "INT",
            parameter.pointSet
                .map { point -> arrayOf(point.abscissa, point.ordinate) }
                .toTypedArray()
        )
        ps.setArray(i, jdbcArray)
    }

    override fun getResult(rs: ResultSet, columnName: String): Coordinates {
        val resultArray = rs.getArray(columnName)?.array as Array<Array<Int>>?
        return resultArray.toCoordinates()
    }

    override fun getResult(rs: ResultSet, columnIndex: Int): Coordinates {
        val resultArray = rs.getArray(columnIndex)?.array as Array<Array<Int>>?
        return resultArray.toCoordinates()
    }

    override fun getResult(cs: CallableStatement, columnIndex: Int): Coordinates {
        val resultArray = cs.getArray(columnIndex)?.array as Array<Array<Int>>?
        return resultArray.toCoordinates()
    }

    private fun Array<Array<Int>>?.toCoordinates(): Coordinates {
        return if (this == null) {
            Coordinates(setOf())
        } else {
            if (this.any { it.size != 2 }) {
                throw IllegalStateException("Unexpected point array size")
            }
            Coordinates(
                this.asSequence()
                    .map { (x, y): Array<Int> -> Point(x, y) }
                    .toSet()
            )
        }
    }
}