package net.battleships.mapper

import net.battleships.domain.GameSettings
import net.battleships.domain.IdOfGame
import net.battleships.domain.IdOfUser
import net.battleships.domain.enumeration.GameModeType
import net.battleships.domain.enumeration.SpectatorModeType
import org.apache.ibatis.annotations.Delete
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

@Mapper
interface GameSettingsMapper {
    // language=sql
    @Delete("TRUNCATE game_settings;")
    fun truncate()

    // language=sql
    @Select(
        """
            SELECT id,
                   game_id gameId,
                   player_id playerId,
                   opponent_id opponentId,
                   timer,
                   game_mode gameMode,
                   spectator_mode spectatorMode
            FROM game_settings;
        """
    )
    fun listAll(): List<GameSettings>

    // language=sql
    @Insert(
        """
            INSERT INTO game_settings(game_id, player_id, opponent_id, timer, game_mode, spectator_mode) 
            VALUES (#{gameId}, #{playerId}, #{opponentId}, #{timer}, #{gameMode}::GAME_MODE, #{spectatorMode}::SPECTATOR_MODE);
        """
    )
    fun save(
        @Param("gameId") gameId: IdOfGame,
        @Param("playerId") playerId: IdOfUser,
        @Param("opponentId") opponentId: IdOfUser,
        @Param("timer") timer: Int,
        @Param("gameMode") gameMode: GameModeType,
        @Param("spectatorMode") spectatorMode: SpectatorModeType,
    )
}