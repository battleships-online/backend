package net.battleships.mapper

import net.battleships.domain.Turn
import net.battleships.domain.enumeration.ActorType
import net.battleships.domain.enumeration.TurnResultType
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

@Mapper
interface TurnHistoryMapper {
    // language=sql
    @Select(
        """
        SELECT id,
               actor,
               abscissa,
               ordinate,
               result,
               turn_number as turnNumber
        FROM turn_history
        """
    )
    fun listAll(): List<Turn>

    // language=sql
    @Insert(
        """
        INSERT INTO turn_history (
            actor, 
            abscissa,
            ordinate, 
            result,
            turn_number
        ) VALUES (
            #{actor}::ACTOR_TYPE,
            #{abscissa},
            #{ordinate},
            #{result}::RESULT_TYPE,
            #{turnNumber}
        )
        """
    )
    fun save(
        @Param("actor") actor: ActorType,
        @Param("abscissa") abscissa: Int?,
        @Param("ordinate") ordinate: Int?,
        @Param("result") result: TurnResultType,
        @Param("turnNumber") turnNumber: Int
    )
}