package net.battleships.mapper

import net.battleships.domain.Arrangement
import net.battleships.domain.Coordinates
import net.battleships.domain.IdOfGame
import net.battleships.domain.IdOfUser
import net.battleships.domain.enumeration.GameObjectType
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

@Mapper
interface ArrangementMapper {
    // language=sql
    @Select(
        """
        SELECT id,
               object_type as objectType, 
               game_id,
               owner_id, 
               coordinates
        FROM game_object_arrangement
        """
    )
    fun listAll(): List<Arrangement>

    // language=sql
    @Insert(
        """
        INSERT INTO game_object_arrangement (
            object_type, 
            game_id,
            owner_id,
            coordinates
        ) VALUES (
            #{objectType}::GAME_OBJECT_TYPE,
            #{ownerId},
            #{gameId},
            #{coordinates}
        )
        """
    )
    fun save(
        @Param("objectType") objectType: GameObjectType,
        @Param("ownerId") ownerId: IdOfUser,
        @Param("gameId") gameId: IdOfGame,
        @Param("coordinates") coordinates: Coordinates
    )
}