package net.battleships.domain

import net.battleships.domain.enumeration.GameModeType
import net.battleships.domain.enumeration.SpectatorModeType

inline class IdOfGameSettings(val id: Long)

data class GameSettings(
    val id: IdOfGameSettings,
    val gameId: IdOfGame,
    val playerId: IdOfUser,
    val opponentId: IdOfUser,
    val timer: Int,
    val gameMode: GameModeType,
    val spectatorMode: SpectatorModeType
)