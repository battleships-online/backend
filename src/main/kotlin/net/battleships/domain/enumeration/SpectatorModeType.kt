package net.battleships.domain.enumeration

enum class SpectatorModeType {
    ALL,
    ONLY_SHOTS,
}