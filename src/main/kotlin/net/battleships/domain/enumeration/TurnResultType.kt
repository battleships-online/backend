package net.battleships.domain.enumeration

enum class TurnResultType {
    MISS,
    HIT,
    SKIP
}