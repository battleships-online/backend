package net.battleships.domain.enumeration

enum class GameObjectType {
    BATTLESHIP,
    DESTROYER,
    CRUISER,
    BOAT
}