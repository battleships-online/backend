package net.battleships.domain.enumeration

enum class ActorType {
    PLAYER,
    OPPONENT,
    GAME_MASTER
}