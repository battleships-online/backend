package net.battleships.domain

data class Coordinates(val pointSet: Set<Point>)