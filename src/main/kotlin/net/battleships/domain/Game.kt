package net.battleships.domain

inline class IdOfGame(val id: Long)