package net.battleships.domain

data class Point(
    val abscissa: Int,
    val ordinate: Int
) {
    companion object {
        fun ofCoordinates(abscissa: Int?, ordinate: Int?): Point? {
            return if (abscissa != null && ordinate != null) {
                Point(abscissa, ordinate)
            } else {
                null
            }
        }
    }
}


