package net.battleships.domain

import net.battleships.domain.enumeration.ActorType
import net.battleships.domain.enumeration.TurnResultType

inline class IdOfTurn(val id: Long)

data class Turn(
    val id: IdOfTurn,
    val actor: ActorType,
    val point: Point?,
    val result: TurnResultType,
    val turnNumber: Int
) {
    constructor(
        id: IdOfTurn,
        actor: ActorType,
        abscissa: Int?,
        ordinate: Int?,
        result: TurnResultType,
        turnNumber: Int
    ) : this(
        id,
        actor,
        Point.ofCoordinates(abscissa, ordinate),
        result,
        turnNumber
    )
}