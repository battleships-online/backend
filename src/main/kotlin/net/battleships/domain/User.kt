package net.battleships.domain

inline class IdOfUser(val id: Long)