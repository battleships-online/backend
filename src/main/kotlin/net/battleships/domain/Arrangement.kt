package net.battleships.domain

import net.battleships.domain.enumeration.GameObjectType

inline class IdOfArrangement(val id: Long)

data class Arrangement(
    val id: IdOfArrangement,
    val objectType: GameObjectType,
    val ownerId: IdOfUser,
    val gameId: IdOfGame,
    val coordinates: Coordinates
)