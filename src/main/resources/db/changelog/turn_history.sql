--liquibase formatted sql

--changeset RIP:issue-1
CREATE TYPE ACTOR_TYPE AS ENUM ('PLAYER', 'OPPONENT', 'GAME_MASTER');

CREATE TYPE RESULT_TYPE AS ENUM ('MISS', 'HIT', 'SKIP');

CREATE TABLE turn_history
(
    id          BIGSERIAL PRIMARY KEY,
    actor       ACTOR_TYPE  NOT NULL,
    abscissa    INT         NULL,
    ordinate    INT         NULL,
    result      RESULT_TYPE NOT NULL,
    turn_number INT         NOT NULL
);

COMMENT ON COLUMN turn_history.abscissa IS 'x coordinate';
COMMENT ON COLUMN turn_history.ordinate IS 'y coordinate';