--liquibase formatted sql

--changeset EGO:issue-2
CREATE TYPE GAME_MODE AS ENUM ('CLASSIC');

CREATE TYPE SPECTATOR_MODE AS ENUM ('ALL', 'ONLY_SHOTS');

CREATE TABLE game_settings
(
    id             BIGSERIAL PRIMARY KEY,
    game_id        BIGINT         NOT NULL UNIQUE,
    player_id      BIGINT         NOT NULL,
    opponent_id    BIGINT         NOT NULL,
    timer          INT            NOT NULL,
    game_mode      GAME_MODE      NOT NULL,
    spectator_mode SPECTATOR_MODE NOT NULL
);