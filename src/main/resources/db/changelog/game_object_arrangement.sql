--liquibase formatted sql

--changeset RIP:issue-1
CREATE TYPE GAME_OBJECT_TYPE AS ENUM ('BATTLESHIP','DESTROYER','CRUISER','BOAT');

CREATE TABLE game_object_arrangement
(
    id          BIGSERIAL PRIMARY KEY,
    object_type GAME_OBJECT_TYPE NOT NULL,
    game_id     bigint           NOT NULL,
    owner_id    bigint           NOT NULL,
    coordinates int[][]          NOT NULL
);