package net.battleships.common

import net.battleships.domain.GameSettings
import net.battleships.domain.IdOfGame
import net.battleships.domain.IdOfGameSettings
import net.battleships.domain.IdOfUser
import net.battleships.domain.enumeration.GameModeType
import net.battleships.domain.enumeration.SpectatorModeType
import org.apache.commons.lang3.RandomUtils

fun generateGameSettings(): GameSettings = GameSettings(
    id = IdOfGameSettings(RandomUtils.nextLong()),
    gameId = IdOfGame(RandomUtils.nextLong()),
    playerId = IdOfUser(RandomUtils.nextLong()),
    opponentId = IdOfUser(RandomUtils.nextLong()),
    timer = RandomUtils.nextInt(),
    gameMode = GameModeType.CLASSIC,
    spectatorMode = SpectatorModeType.ALL,
)