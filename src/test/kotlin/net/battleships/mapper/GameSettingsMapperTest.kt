package net.battleships.mapper

import net.battleships.common.generateGameSettings
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@SpringBootTest
@ExtendWith(SpringExtension::class)
class GameSettingsMapperTest(
    @Autowired private val gameSettingsMapper: GameSettingsMapper
) {

    @BeforeEach
    fun beforeEach() {
        gameSettingsMapper.truncate()
    }

    @Test
    fun `create game settings`() {
        // given
        val expected = generateGameSettings()

        gameSettingsMapper.save(
            expected.gameId,
            expected.playerId,
            expected.opponentId,
            expected.timer,
            expected.gameMode,
            expected.spectatorMode,
        )

        // when
        val actual = gameSettingsMapper.listAll()

        // then
        assertThat(actual)
            .usingRecursiveFieldByFieldElementComparatorIgnoringFields("id")
            .containsExactly(expected)
    }
}