#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username postgres --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE DATABASE battleships;
  CREATE ROLE battleships WITH LOGIN PASSWORD 'password';
  GRANT ALL PRIVILEGES ON DATABASE battleships TO battleships;
EOSQL
